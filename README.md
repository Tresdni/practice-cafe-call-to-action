**Installation**

1. cd to wp-content/plugins folder

2.  Execute (without quotes) "git clone https://Tresdni@bitbucket.org/Tresdni/practice-cafe-call-to-action.git calltoaction"

**Example Usage**

[calltoaction text="Some cool text!" href="/" button_text="Push Me"]

**Notes**

Styles and images are contained within the plugin's folder itself.  I've included a sass version of the style sheet as well.