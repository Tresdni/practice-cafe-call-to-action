<?php
/**
 * @package Practice Cafe Call To Action
 */
/*
Plugin Name: Practice Cafe Call To Action
Plugin URI: http://www.practicecafe.com
Description: Add a call to action to your site by simply using a [calltoaction] short code.
Author: Kalan Brock
Author URI: http://www.practicecafe.com
License: GPLv2 or later
Text Domain: calltoactionpracticecafe
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

function call_to_action_styles() {
	wp_enqueue_style( 'calltoaction', plugins_url( 'css/calltoaction.css', __FILE__ ) );
}

add_action( 'wp_enqueue_scripts', 'call_to_action_styles' );

function call_to_action($atts) 
{
	$text = (isset($atts['text'])) ? $atts['text'] : '';
	$href = (isset($atts['href'])) ? $atts['href'] : '';
	$button_text = (isset($atts['button_text'])) ? $atts['button_text'] : '';

	$html = '
		<div class="contact_us_button_element">
			<div class="inner">
				<div class="text">'.$text.'</div>
				<div class="button_wrap">
					<a class="contact_button" href="'.$href.'">'.$button_text.'</a>
				</div>
			</div>
		</div>
	';

	return $html;
}

add_shortcode( 'calltoaction', 'call_to_action' );
